module.exports = {
  arrowParens: 'always',
  bracketSpacing: true,
  proseWrap: 'always',
  singleQuote: true,
  trailingComma: 'all',
};
