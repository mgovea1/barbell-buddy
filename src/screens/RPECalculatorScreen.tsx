import * as React from 'react';
import { StyleSheet } from 'react-native';
import { IterableX as Iterable } from '@reactivex/ix-ts/iterable';
import '@reactivex/ix-ts/add/iterable/range';
import '@reactivex/ix-ts/add/iterable-operators/map';

import { ScrollView, Text, TextInput, View } from '../components/Themed';
import Colors from '../constants/Colors';
import E1RMConversion from '../math/E1RMConversion';

const defaultWeight = 70;
const defaultReps = 5;
const defaultRPE = 8;

export default function RPECalculatorScreen() {
  const [weight, setWeight] = React.useState('');
  const [reps, setReps] = React.useState('');
  const [rpe, setRPE] = React.useState('');

  const [e1RM, validInput] = _getE1RM(Number(weight), Number(reps), Number(rpe));
  return (
    <View style={styles.container}>
      <ScrollView
        style={styles.container}
        contentContainerStyle={styles.contentContainer}
      >
        <View style={styles.multitextOuterContainer}>
          <View style={styles.multitextInnerContainer}>
            {/* TODO: validate input & perhaps highlight bad numbers */}
            {_renderNumbericInput(
              weight,
              String(defaultWeight),
              (weight) => setWeight(weight),
            )}
            <Text style={styles.inputSectionText}>for</Text>
            {_renderNumbericInput(
              reps,
              String(defaultReps),
              (reps) => setReps(reps),
            )}
            <Text style={styles.inputSectionText}>reps at RPE</Text>
            {_renderNumbericInput(
              rpe,
              String(defaultRPE),
              (rpe) => setRPE(rpe),
            )}
          </View>
        </View>

        <View style={styles.multitextOuterContainer}>
          <View style={styles.multitextInnerContainer}>
            <Text style={styles.e1RMSectionText}>e1RM:</Text>
            <Text
              style={styles.e1RMSectionText}
              {...(validInput ? {} : e1RMInvalidTextTheme)}
            >
              {e1RM.toFixed(1)}
            </Text>
          </View>
        </View>

        <View style={styles.tableContainer}>{_renderRPETable(e1RM)}</View>
      </ScrollView>
    </View>
  );
}

function _renderNumbericInput(
  value: string,
  placeholder: string,
  callback: (s: string) => void,
): JSX.Element {
  return (
    <TextInput
      style={[styles.inputSectionText, styles.inputSectionTextInput]}
      keyboardType="numeric"
      multiline={false}
      onChangeText={callback}
      placeholder={placeholder}
      selectTextOnFocus={true}
      value={value}
    />
  );
}

function _renderRPETable(e1RM: number): ReadonlyArray<JSX.Element> {
  const cell = (text: string | number) => (
    <View style={styles.tableCell}>
      <Text>{text}</Text>
    </View>
  );
  return [
    <View key={'RPETableHeader' + new Date()} style={styles.tableRow}>
      {cell('Reps')}
      {cell('RPE 10')}
      {cell('RPE 9')}
      {cell('RPE 8')}
      {cell('RPE 7')}
      {cell('RPE 6')}
    </View>,
    ...Iterable.range(1, 16).map((reps: number) => (
      <View
        key={'RPETableRow' + reps + String(e1RM.toFixed(2)) + new Date()}
        style={styles.tableRow}
      >
        {cell(reps)}
        {cell(E1RMConversion.fromE1RM(e1RM, reps, 10).toFixed(1))}
        {cell(E1RMConversion.fromE1RM(e1RM, reps, 9).toFixed(1))}
        {cell(E1RMConversion.fromE1RM(e1RM, reps, 8).toFixed(1))}
        {cell(E1RMConversion.fromE1RM(e1RM, reps, 7).toFixed(1))}
        {cell(E1RMConversion.fromE1RM(e1RM, reps, 6).toFixed(1))}
      </View>
    )),
  ];
}

function _getE1RM(weight: number, reps: number, rpe: number): [number, boolean] {
  let correctInput = true;
  if (isNaN(weight) || weight <= 0) {
    weight = defaultWeight;
    correctInput = false;
  }
  if (isNaN(reps) || reps <= 0) {
    reps = defaultReps;
    correctInput = false;
  }
  if (isNaN(rpe) || rpe < 4) {
    rpe = defaultRPE;
    correctInput = false;
  }
  if (!E1RMConversion.inComputableRange(reps, rpe)) {
    reps = defaultReps;
    rpe = defaultRPE;
    correctInput = false;
  }

  // TODO notify if difficulty too low (high rep, low rpe)

  return [E1RMConversion.toE1RM(weight, reps, rpe), correctInput];
}

const e1RMInvalidTextTheme = {
  lightColors: {text: Colors.light.placeholderText},
  darkColors: {text: Colors.dark.placeholderText},
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  contentContainer: {
    paddingTop: 30,
  },

  e1RMSectionText: {
    fontSize: 40,
    marginLeft: 4,
    marginRight: 4,
  },

  inputSectionText: {
    fontSize: 26,
    marginLeft: 6,
    marginRight: 6,
  },
  inputSectionTextInput: {
    borderBottomWidth: 1,
  },

  multitextInnerContainer: {
    flexDirection: 'row',
  },
  multitextOuterContainer: {
    alignItems: 'center',
    marginBottom: 10,
    marginTop: 10,
  },

  tableCell: {
    alignItems: 'center',
    alignSelf: 'stretch',
    flex: 1,
  },
  tableContainer: {
    marginBottom: 10,
    marginTop: 10,
  },
  tableRow: {
    alignSelf: 'stretch',
    flex: 1,
    flexDirection: 'row',
    margin: 4,
  },
});
