import * as React from 'react';
import {
  ScrollView as DefaultScrollView,
  Text as DefaultText,
  TextInput as DefaultTextInput,
  View as DefaultView,
} from 'react-native';

import Colors, { ThemedColor } from '../constants/Colors';
import useColorScheme from '../hooks/useColorScheme';

export function useThemeColor(
  props: { light?: ThemePropEntry; dark?: ThemePropEntry },
  colorName: ThemedColor,
) {
  const theme = useColorScheme();
  const colorFromProps = props[theme];

  if (colorFromProps && colorFromProps[colorName]) {
    return colorFromProps[colorName];
  } else {
    return Colors[theme][colorName];
  }
};

type ThemePropEntry = {[key in ThemedColor]?: string};
type ThemeProps = {
  lightColors?: ThemePropEntry;
  darkColors?: ThemePropEntry;
};

export type ScrollViewProps = ThemeProps & DefaultScrollView['props'];
export function ScrollView(props: ScrollViewProps) {
  const { style, lightColors, darkColors, ...otherProps } = props;
  const backgroundColor = useThemeColor({ light: lightColors, dark: darkColors }, 'background');

  return <DefaultScrollView style={[{ backgroundColor }, style]} {...otherProps} />;
}

export type TextProps = ThemeProps & DefaultText['props'];
export function Text(props: TextProps) {
  const { style, lightColors, darkColors, ...otherProps } = props;
  const color = useThemeColor({ light: lightColors, dark: darkColors }, 'text');

  return <DefaultText style={[{ color }, style]} {...otherProps} />;
}

export type TextInputProps = ThemeProps & DefaultTextInput['props'];
export function TextInput(props: TextInputProps) {
  const { style, lightColors, darkColors, ...otherProps } = props;
  const borderBottomColor = useThemeColor({ light: lightColors, dark: darkColors }, 'textInputBottomBorder');
  const color = useThemeColor({ light: lightColors, dark: darkColors }, 'text');
  const placeholderTextColor = useThemeColor({ light: lightColors, dark: darkColors }, 'placeholderText');

  return (
    <DefaultTextInput
      style={[{ borderBottomColor, color }, style]}
      placeholderTextColor={placeholderTextColor}
      {...otherProps}
    />
  );
}

export type ViewProps = ThemeProps & DefaultView['props'];
export function View(props: ViewProps) {
  const { style, lightColors, darkColors, ...otherProps } = props;
  const backgroundColor = useThemeColor({ light: lightColors, dark: darkColors }, 'background');

  return <DefaultView style={[{ backgroundColor }, style]} {...otherProps} />;
}
