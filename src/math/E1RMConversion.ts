export default class E1RMConversion {
  public static inComputableRange(reps: number, rpe: number): boolean {
    const difficulty = E1RMConversion.getPercentageIndex(reps, rpe);
    return (
      -0.1 < difficulty && difficulty <= E1RMConversion.percentageList.length - 1
    );
  }

  public static fromE1RM(e1RM: number, reps: number, rpe: number): number {
    return e1RM * E1RMConversion.getPercentageFactor(reps, rpe);
  }

  public static toE1RM(weight: number, reps: number, rpe: number): number {
    return weight / E1RMConversion.getPercentageFactor(reps, rpe);
  }

  public static getPercentageFactor(reps: number, rpe: number): number {
    let difficulty = E1RMConversion.getPercentageIndex(reps, rpe);
    if (difficulty < 0) {
      // TODO: yell if it's less than -0.1?
      difficulty = 0;
    } else if (difficulty > E1RMConversion.percentageList.length - 1) {
      difficulty = E1RMConversion.percentageList.length - 1;
    }

    if (Number.isInteger(difficulty)) {
      return E1RMConversion.percentageList[difficulty];
    } else {
      // Linearly interpolate.
      const index = Math.floor(difficulty);
      const offset = difficulty - index;
      return (
        E1RMConversion.percentageList[index] * (1 - offset) +
        E1RMConversion.percentageList[index + 1] * offset
      );
    }
  }

  private static getPercentageIndex(reps: number, rpe: number): number {
    return (reps - rpe + 9) * 2;
  }

  // This is RTS's chart from 1RM to 8RM
  // then 2.6% drops for each REP after that (1.3% per 0.5 RPE)
  private static readonly percentageList: ReadonlyArray<number> = [
    1.0, // 1 @ 10
    0.978, // 1 @ 9.5
    0.955, // 1 @ 9 or 2 @ 10
    0.939,
    0.922, // 1 @ 8, 2 @ 9, 3 @ 10
    0.907,
    0.892,
    0.878,
    0.863,
    0.85,
    0.837,
    0.824,
    0.811,
    0.799,
    0.786, // 8 @ 10
    0.773, // This is the first deviation
    0.76,
    0.747,
    0.734,
    0.721,
    0.708,
    0.695,
    0.682,
    0.669,
    0.656,
    0.643,
    0.63,
    0.617,
    0.604,
    0.591,
    0.578,
    0.565,
    0.552,
    0.539,
    0.526,
    0.513,
    0.5,
    0.487,
    0.474,
  ];

  // The original RTS list
  // 1.0, // 1 @ 10
  // 0.978, // 1 @ 9.5
  // 0.955, // 1 @ 9 or 2 @ 10
  // 0.939,
  // 0.922, // 1 @ 8, 2 @ 9, 3 @ 10
  // 0.907,
  // 0.892,
  // 0.878,
  // 0.863,
  // 0.85,
  // 0.837,
  // 0.824,
  // 0.811,
  // 0.799,
  // 0.786,
  // 0.774,
  // 0.762,
  // 0.751,
  // 0.739,
  // 0.723,
  // 0.707,
  // 0.694,
  // 0.68,
  // 0.667,
  // 0.653,
  // 0.64,
  // 0.626,
  // 0.613,
  // 0.599,
  // 0.586,
  // 0.573, // I MADE THIS ONE UP by extrapolating from the last 2 to render 12 @ 6
}
