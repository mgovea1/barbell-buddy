export type RootStackParamList = {
  Root: undefined;
  NotFound: undefined;
};

export type BottomTabParamList = {
  "RPE Calculator": undefined;
  TabTwo: undefined;
};

export type RPECalculatorParamList = {
  RPECalculatorScreen: undefined;
};

export type TabTwoParamList = {
  TabTwoScreen: undefined;
};
