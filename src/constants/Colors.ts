const tintColorLight = '#2f95dc';
const tintColorDark = '#fff';
const textColorLight = '#000';
const textColorDark = '#fff';

const Colors = {
  light: {
    text: textColorLight,
    background: '#fff',
    placeholderText: 'lightgray',
    tint: tintColorLight,
    tabIconDefault: '#ccc',
    tabIconSelected: tintColorLight,
    textInputBottomBorder: textColorLight,
  },
  dark: {
    text: textColorDark,
    background: '#000',
    placeholderText: 'gray',
    tint: tintColorDark,
    tabIconDefault: '#ccc',
    tabIconSelected: tintColorDark,
    textInputBottomBorder: textColorDark,
  },
};

export default Colors;

type ThemedColor = keyof typeof Colors.light & keyof typeof Colors.dark;

export { ThemedColor };
